Name:           plasma-mediacenter
Version:        5.7.5
Release:        2
Summary:        A mediacenter user interface written with the Plasma framework

License:        GPLv2
URL:            https://cgit.kde.org/plasma-mediacenter.git

%global revision %(echo %{version} | cut -d. -f3)
%if %{revision} >= 50
%global stable unstable
%else
%global stable stable
%endif
Source0:        http://download.kde.org/%{stable}/plasma/%{version}/%{name}-%{version}.tar.xz
Patch0:         plasma-mediacenter-5.7.5-ctaglib.patch

BuildRequires:  desktop-file-utils
BuildRequires:  extra-cmake-modules
BuildRequires:  kf5-baloo-devel
BuildRequires:  kf5-kactivities-devel
BuildRequires:  kf5-kconfig-devel
BuildRequires:  kf5-kcoreaddons-devel
BuildRequires:  kf5-kdeclarative-devel
BuildRequires:  kf5-kguiaddons-devel
BuildRequires:  kf5-ki18n-devel
BuildRequires:  kf5-kio-devel
BuildRequires:  kf5-kservice-devel
BuildRequires:  kf5-plasma-devel
BuildRequires:  kf5-rpm-macros
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtdeclarative-devel
BuildRequires:  qt5-qtxmlpatterns-devel
BuildRequires:  taglib-devel

Obsoletes:      plasma-mediacenter-devel < 5.0.0

Requires:       qt5-qtmultimedia%{?_isa}

%description
Plasma Media Center is designed to provide an easy and comfortable
way to watch your videos, browse your photo collection and listen to
your music, all in one place. This release brings many refinements
and a host of new features, making consuming media even easier and
more fun.


%prep
%autosetup -p1


%build
%{cmake_kf5}
%cmake_build


%install
%cmake_install

%find_lang all --all-name


%check
desktop-file-validate %{buildroot}%{_kf5_datadir}/applications/plasma-mediacenter.desktop


%ldconfig_scriptlets

%files -f all.lang
%license COPYING COPYING.LIB
%doc README
%{_kf5_libdir}/libplasmamediacenter.so.*
%{_kf5_qtplugindir}/plasma/mediacenter/
%{_kf5_qmldir}/org/kde/plasma/mediacenter/
%{_kf5_datadir}/applications/plasma-mediacenter.desktop
%{_kf5_datadir}/kservices5/plasma-shell-org.kde.plasma.mediacenter.desktop
%{_kf5_metainfodir}/org.kde.plasma.mediacenter.appdata.xml
%{_kf5_datadir}/kservicetypes5/*.desktop
%{_kf5_datadir}/plasma/shells/org.kde.plasma.mediacenter/
%{_datadir}/xsessions/plasma-mediacenter.desktop
%{_datadir}/icons/hicolor/*/*/*


%changelog
* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 5.7.5-2
- adapt to the new CMake macros to fix build failure

* Tue Jul 19 2022 misaka00251 <misaka00251@misakanet.cn> - 5.7.5-1
- Init package
